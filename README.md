# Solución propuesta a la TP2

El presente repositorio contine código que sirve como referencia para solucionar la tarea programada II.

* El modelo que se propone (utilizando UML) es el siguiente:

    ![diagrama](http://drive.google.com/uc?export=view&id=1ggK0Ma304tU4PpZVsxtTRIm8T8mf5-Bo)
    
* Básicamente se descompone el problema en 4 partes:

    1.  Manejo de archivos (csv)
    2.  Manejo de datos (matrices)
    3.  La aplicación (utiliza la matriz)
    4.  La vista de la app
    
    De ahí que el código se encuentre empaquetado de esta forma.

* Se implementaron los algoritmos para las partes 1, 2, y 3. 
* La GUI queda a gusto del estudiante, así como implementar diferentes operaciones aritméticas con los datos.

---

## Web site con la documentación JAVADOC

La lectura de ![documentación](https://es.wikipedia.org/wiki/Javadoc) es siempre importante en desarrollo de software. Ofrece una mejor comprensión de los diseños.

***Ver la documentación del código dando ![click aquí](https://carlos401.gitlab.io/calcapplication)***

---

## Aclaración importante
> Recuerden que este es solo uno de los muchos diseños posibles. 
> Un mismo problema tiene diferentes soluciones. 
> Este repositorio solo se ofrece como un medio de ampliar el conocimiento y de considerar ideas no pensadas antes.

---

# Licencia
CalcApplication
    Copyright (C) 2018  Carlos Delgado

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.