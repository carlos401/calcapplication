//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package File;

import Data.Cell;
import Data.Matrix;

import javax.swing.*;
import java.io.*;
import java.util.StringTokenizer;

/**
 * This class allows to manage csv files
 * @author carlos401
 */
public class CSVManager {
    //to choose file
    private JFileChooser jFileChooser;

    /**
     * Default constructor
     */
    public CSVManager() {
        this.jFileChooser = new JFileChooser();
        this.jFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    }

    /**
     * Loads file and put it into Matrix object
     * @return the data in Matrix format
     */
    public Matrix loadFile(){
        int result = jFileChooser.showOpenDialog(null);
        switch (result){
            case JFileChooser.CANCEL_OPTION:
                return null;
            case JFileChooser.APPROVE_OPTION:
                return putDataInMatrix(this.jFileChooser.getSelectedFile());
            default:
                return null;
        }
    }

    /**
     * Converts File into Matrix
     * @param file the file to converts
     * @return the matrix equivalent, null in case of problems
     */
    private Matrix putDataInMatrix(File file){
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(file));
            Matrix matrix = new Matrix(8,8); //size could change
            String completeRow; //data
            int row = 1; //number
            while ((completeRow = in.readLine())!= null)
            {
                int column =1;
                StringTokenizer st = new StringTokenizer(completeRow, ";");
                while (st.hasMoreTokens()){
                    String cellValue = st.nextToken();
                    matrix.addValue(new Cell(Cell.getDataType(cellValue),cellValue),row,column);
                    column++;
                }
                row++;
            }
            in.close();
            return matrix;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Puts matrix into file
     * @param data the matrix of data
     * @return true if the process was successful
     */
    public boolean saveFile(Matrix data){
        int result = jFileChooser.showOpenDialog(null);
        switch (result){
            case JFileChooser.CANCEL_OPTION:
                return false;
            case JFileChooser.APPROVE_OPTION:
                return putDataInFile(jFileChooser.getSelectedFile(),data);
            default:
                return false;
        }
    }

    /**
     * Puts matrix into file
     * @param file file to edit
     * @param data data to save into the file
     */
    private boolean putDataInFile(File file, Matrix data){
        try
        {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            for (int row=1; row <= data.getRows(); row++) {
                for (int column=1 ; column <= data.getColumns(); column++) {
                    out.write(data.getValue(row,column).getData()+";");
                    out.flush();
                }
                out.newLine();
            }
            out.close();
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) {
        CSVManager csvManager = new CSVManager();

        // pruebas para carga de arhivos
        Matrix matrix = csvManager.loadFile();
        for(int i=1; i<=matrix.getRows(); ++i){
            for (int j=1; j<=matrix.getColumns();++j){
                System.out.println("Valor: " + matrix.getValue(i,j).getData());
                System.out.println("Tipo: " + matrix.getValue(i,j).getDataType());
            }
        }

        // pruebas para guardado de datos
        //Matrix matrix = new Matrix(2,2);
        //matrix.addValue(new Cell(DataType.NUMERIC_VALUE,"2"),0,0);
        //matrix.addValue(new Cell(DataType.NUMERIC_VALUE,"3"),0,1);
        //matrix.addValue(new Cell(DataType.NUMERIC_VALUE,"4"),1,0);
        //matrix.addValue(new Cell(DataType.NUMERIC_VALUE,"5"),1,1);
        //csvManager.saveFile(matrix);
    }
}
