//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package View;

import Application.CalcApplication;

import javax.swing.*;

/**
 * This class executes GUI for the app
 */
public class Screen {
    private JPanel panel1;
    private JTable table1;
    private JButton insertRowButton;
    private JButton deleteColumnButton;
    private JButton importFileButton;
    private JButton saveFileButton;
    private JTextField textField1;

    private CalcApplication application;

    // ADD all your GUI methods here using the app

    public static void main(String[] args) {
        Screen screen = new Screen();
    }
}
