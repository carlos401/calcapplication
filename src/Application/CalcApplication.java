//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package Application;

import Data.Cell;
import Data.DataType;
import Data.Matrix;
import File.CSVManager;

import java.util.StringTokenizer;

import static Data.DataType.FORMULA;
import static Data.DataType.STRING_VALUE;

/**
 * This class is the principal controller
 * @author carlos401
 */
public class CalcApplication {

    // matrix with data
    private Matrix sheet;

    // to manage csv files
    private CSVManager fileManager;

    /**
     * Constructor
     */
    public CalcApplication() {
        this.sheet = new Matrix(8,8); //dimensions can change
        this.fileManager = new CSVManager();
    }

    /**
     * Loads data from file and put it in the matrix
     * @return true if the loading process was successful, false otherwise
     */
    public boolean loadSheet(){
        this.sheet = this.fileManager.loadFile();
        return this.sheet != null;
    }

    /**
     * Saves data from the matrix to file
     * @return true if the saving process was successful, false otherwise
     */
    public boolean saveSheet(){
        return this.fileManager.saveFile(this.sheet);
    }

    /**
     * Applies an operation into an specific cell
     * @param row number from 1 to 8
     * @param column number from 1 to 8 (A, B, C ...)
     * @param data the operation, can be a number, string or formula
     * @return the number or string related to the data param
     */
    public String applyOperation(int row, int column, String data){
        //saves the value
        DataType dataType = Cell.getDataType(data);
        this.sheet.addValue(new Cell(dataType,data),row,column);

        // now apply operations
        if(dataType==FORMULA) { //formulas should be processed
            //clean the input
            data = data.replace("=",":");
            data =data.replace('(', ':');
            data = data.replace(')', ':');

            try {
                StringTokenizer st = new StringTokenizer(data, ":");
                //Examp: =sum(a2:a5) >>> sum a2 a5

                String operation = st.nextToken();
                switch (operation) {
                    case "sum":
                        String beg = st.nextToken();
                        String end = st.nextToken();
                        return this.sum(beg,end);

                    // more operations can be added here
                    // for example:
                    //
                    //case "average":
                    //  String beg = st.nextToken();
                    //  String end = st.nextToken();
                    //  return this.average(beg,end);
                    default:
                        return "INVALID OPERATION";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "SINTAX ERROR";
            }
        }
        else {
            //numeric or strings data
            return data;
        }
    }

    /**
     * Applies the sum formula
     * @param beg beginning cell. Ex: "a1"
     * @param end final cell Ex: "f1"
     * @return the value of the operation, Error string in case of problems
     */
    private String sum(String beg, String end){
        //parse the input
        int rowBeg = beg.charAt(1) - 48; //traduce char number to int
        int columnBeg = beg.charAt(0) - 96; //traduce char letter to int
        int rowEnd = end.charAt(1) - 48;
        int columnEnd = end.charAt(0) - 96;

        //result to be returned
        int sum = 0;

        try{
            if(columnBeg < columnEnd){ //row sum
                for (int j = columnBeg; j<= columnEnd; j++){
                    String number = this.sheet.getValue(rowBeg,j).getData();
                    sum += Integer.parseInt(number);
                }
            } else if(rowBeg < rowEnd){ //column sum
                for(int i = rowBeg; i<=rowEnd; i++){
                    String number = this.sheet.getValue(i,columnBeg).getData();
                    sum += Integer.parseInt(number);
                }
            } else {
                return "INVALID PARAMS";
            }
            return ""+sum;
        } catch (Exception e){
            e.printStackTrace();
            return "SUM ERROR";
        }
    }

    /**
     * Applies the insert row operation into the matrix
     * @param baseRow base number of row
     * @return true if the insertion process was successful, false otherwise
     */
    public boolean insertRow(int baseRow){
        try{
            //copy values bottom-up
            for(int i=sheet.getRows(); i>baseRow; --i){
                for(int j=1; j<=this.sheet.getColumns(); ++j){
                    Cell copy = this.sheet.getValue(i-1,j);
                    this.sheet.addValue(copy,i,j);
                }
            }
            //clean the inserted row
            for(int i=1; i<=this.sheet.getColumns(); i++){
                Cell emptyCell = new Cell(STRING_VALUE,"");
                this.sheet.addValue(emptyCell,baseRow,i);
            }
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Applies the delete column operation into the matrix
     * @param column the number of column to be deleted
     * @return rue if the deleting process was successful, false otherwise
     */
    public boolean deletecolumn(int column){
        try{
            // move values right to left into the rows
            for(int i=1; i<this.sheet.getRows(); i++){
                for (int j=column; j<this.sheet.getColumns();j++){
                    Cell copy = this.sheet.getValue(i,j+1);
                    this.sheet.addValue(copy,i,j);
                }
                // the last column now is empty
                Cell emptyCell = new Cell(STRING_VALUE,"");
                this.sheet.addValue(emptyCell,i,this.sheet.getColumns());
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public static void main(String[] args) {
        CalcApplication calcApplication = new CalcApplication();
        calcApplication.loadSheet();
        //System.out.println(calcApplication.applyOperation(4, 2, "=sum(a1:a2)"));
        calcApplication.deletecolumn(3);
        calcApplication.saveSheet();
    }

}
