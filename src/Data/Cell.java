//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package Data;

/**
 * This class represents the content in the matrix
 */
public class Cell {

    //Data type in the cell
    private DataType dataType;

    //The data itself
    private String data;

    /**
     * Constructor
     * @param dataType the data type
     * @param data the data
     */
    public Cell(DataType dataType, String data) {
        this.dataType = dataType;
        this.data = data;
    }

    /**
     * Returns the data type
     * @return the data type
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Sets the data type
     * @param dataType new data type
     */
    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    /**
     * Returns the data
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * Sets the data
     * @param data new data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Allows to know the data type based on the string format
     * @param input the string
     * @return the data type for the specified input
     */
    public static DataType getDataType(String input){
        try{
            //try to convert the String into decimal
            Integer.parseInt(input);
            return DataType.NUMERIC_VALUE;
        } catch (Exception e){
            // "=" at the beg, means formula
            if(input.charAt(0)=='='){
                return DataType.FORMULA;
            } else {
                return DataType.STRING_VALUE;
            }
        }
    }
}
