//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package Data;

import static Data.DataType.*;

/**
 * This class stores data and manages matrix data structure
 * @author carlos401
 */
public class Matrix {

    //data
    private Cell[][] data;

    //max number of columns
    private int columns;

    //max number of rows
    private int rows;

    /**
     * Constructor
     * @param columns max number of columns
     * @param rows max number of rows
     */
    public Matrix(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
        this.data = new Cell[rows+1][columns+1];
        //initialize the matrix
        for(int i=1; i<=rows; ++i){
            for (int j=1; j<=columns;++j){
                data[i][j] = new Cell(STRING_VALUE,"");
            }
        }
    }

    /**
     * Returns the complete matrix
     * @return the matrix of cells
     */
    public Cell[][] getData() {
        return data;
    }

    /**
     * Sets the data
     * @param data new matrix of cells
     */
    public void setData(Cell[][] data) {
        this.data = data;
    }

    /**
     * Returns the max number of columns
     * @return the max number of columns
     */
    public int getColumns() {
        return columns;
    }

    /**
     * Sets the number of columns
     * @param columns new dimension
     */
    public void setColumns(int columns) {
        this.columns = columns;
    }

    /**
     * Returns the max number of rows
     * @return the max number of rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets the number of rows
     * @param rows new dimension
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Modifies the value in an specific position
     * @param value new value
     * @param row number of row
     * @param column number of column
     * @return true if the process was successful, false otherwise
     */
    public boolean addValue(Cell value, int row, int column){
        try{
            this.data[row][column] = value;
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Deletes the value in an specific position
     * @param row number of row
     * @param column number of column
     * @return true if the process was successful, false otherwise
     */
    public Cell deleteValue(int row, int column){
        try{
            Cell oldValue = this.data[row][column];
            this.data[row][column].setData("");
            this.data[row][column].setDataType(STRING_VALUE);
            return oldValue;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the value in the position
     * @param row number of row
     * @param column number of column
     * @return the value
     */
    public Cell getValue(int row, int column){
        try{
            return this.data[row][column];
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
