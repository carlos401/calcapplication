//  >> UNIVERSIDAD DE COSTA RICA
//  >> ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
//  >> CURSO PROGRAMACIÓN 1, II 2018

package Data;

/**
 * This enum defines data types in cells
 * @author carlos401
 */
public enum DataType {
    /**
     * Standard strings
     */
    STRING_VALUE,

    /**
     * Numerical values. Ex: 1, 4, 737, etc
     */
    NUMERIC_VALUE,

    /**
     * Input with "=" character at the beg
     */
    FORMULA
}
